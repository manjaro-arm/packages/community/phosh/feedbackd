# Maintainer: Philip Müller <philm@manjaro.org>
# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Dan Johansen
# Contributor: Danct12 <danct12@disroot.org>
# Contributor: Jelle van der Waa <jelle@archlinux.org>
# Contributor: Thomas Booker (CoderThomasB) <tw.booker@outlook.com>
# Contributor: <reg-archlinux AT klein DOT tuxli DOT ch>
# Contributor: Philip Goto <philip.goto@gmail.com>
# Contributor: Sam Whited <sam@samwhited.com>

pkgname=feedbackd
pkgver=0.7.0
pkgrel=1
pkgdesc="A daemon to provide haptic feedback on events"
url="https://source.puri.sm/Librem5/feedbackd"
arch=('x86_64' 'armv7h' 'aarch64')
license=('GPL-3.0-or-later')
depends=(
  'dconf'
  'gsettings-desktop-schemas'
  'gsound'
  'json-glib'
  'libgudev'
)
makedepends=(
  'git'
  'glib2-devel'
  'gobject-introspection'
  'libgmobile'
  'meson'
  'python-docutils'
  'vala'
)
checkdepends=('umockdev')
optdepends=('feedbackd-device-themes: Device specific themes')
provides=('libfeedback-0.0.so=0')
install="$pkgname.install"
source=("git+https://source.puri.sm/Librem5/feedbackd.git#tag=v$pkgver")
sha256sums=('60cb55ba211bf3b83727bbd26aab064213c2ce03d11cbb2f02c789c3b7f2bd2b')

prepare() {
  cd "$pkgname"
  sed -i 's/libexecdir/libdir/g' \
    meson.build data/meson.build \
    data/org.sigxcpu.Feedback.service.in \
    src/meson.build
  sed -i 's/libexec/lib/g' "data/90-$pkgname.rules"
}

build() {
  arch-meson "$pkgname" build -Dman=true
  meson compile -C build
}

check() {
  meson test -C build --no-rebuild --print-errorlogs
}

package() {
  meson install -C build --no-rebuild --destdir "$pkgdir"
}
